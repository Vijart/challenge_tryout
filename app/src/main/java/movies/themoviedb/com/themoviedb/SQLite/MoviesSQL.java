package movies.themoviedb.com.themoviedb.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Victor Reyes on 27/3/2017.
 */

public class MoviesSQL extends SQLiteOpenHelper {

    private String sqlCreateMovies = "CREATE TABLE IF NOT EXISTS Movies( " +
            "id INTEGER,"+
            "posterPath TEXT,"+
            "overview 		TEXT," +
            "releaseDate 		TEXT," +
            "original_title      TEXT,"+
            "original_languaje      TEXT,"+
            "title       TEXT,"+
            "popularity    REAL,"+
            "vote_average       TEXT,"+
            "PRIMARY KEY (id) )";

    private String sqlCreateGenres = "CREATE TABLE IF NOT EXISTS Genres( " +
            "idMovie INTEGER,"+
            "genres_txt TEXT )";

    public MoviesSQL(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateMovies);
        db.execSQL(sqlCreateGenres);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(sqlCreateMovies);
        db.execSQL(sqlCreateGenres);
    }
}
