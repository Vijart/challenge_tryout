package movies.themoviedb.com.themoviedb.Interfaces;

import java.util.List;

/**
 * Created by Victor Reyes on 22/3/2017.
 */

public interface CategoriesView {

    //void showProgress();
    //void hideProgress();

    void showProgress(boolean opt);
    void showError(String Err);
    //void showResult(String Result);
    void setItems(List<String> items);
    void showMessage(String message);

}
