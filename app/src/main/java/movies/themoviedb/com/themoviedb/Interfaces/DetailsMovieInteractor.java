package movies.themoviedb.com.themoviedb.Interfaces;

import org.json.JSONArray;

import java.util.ArrayList;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import movies.themoviedb.com.themoviedb.Entidades.Result;

/**
 * Created by Victor Reyes on 25/3/2017.
 */

public interface DetailsMovieInteractor {

    interface OnDetailsDataMovie{
        void onDetails(Result results);
    }

    void findMoviePosition(OnDetailsDataMovie listener,int position, String URL);

}
