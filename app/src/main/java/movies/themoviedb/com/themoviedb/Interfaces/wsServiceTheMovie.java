package movies.themoviedb.com.themoviedb.Interfaces;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import rx.Observable;

/**
 * Created by Victor Reyes on 23/3/2017.
 */

public interface wsServiceTheMovie {

    Observable<ResponseTheMovie> getDataTheMovieService();

}
