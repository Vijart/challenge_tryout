package movies.themoviedb.com.themoviedb.Interfaces;

/**
 * Created by Victor Reyes on 22/3/2017.
 */

public interface CategoriesPresenter {

    //El presentador es el puente entre la vista y el interactor
    void showMsgPresenter(String Toast);
    //void showResultPresenter(String Result);
    void charge_dataBase();
    void getDataURL(String URL);
    void onItemClicked(int position);
    //void webServiceURL(String URL);
}
