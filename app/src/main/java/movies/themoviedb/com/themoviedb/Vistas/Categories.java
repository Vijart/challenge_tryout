package movies.themoviedb.com.themoviedb.Vistas;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.List;

import movies.themoviedb.com.themoviedb.Interfaces.CategoriesPresenter;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesView;
import movies.themoviedb.com.themoviedb.Presentadores.CategoriesPresenterImpl;
import movies.themoviedb.com.themoviedb.R;


public class Categories extends Activity implements CategoriesView, AdapterView.OnItemClickListener{

    private ProgressBar progressBar;
    //private TextView result;
    private ListView result;
    private View okHttp;
    private CategoriesPresenter presenter;

    private Resources res;
    private TabHost tabs; // Control del TabHost
    private TabHost.TabSpec spec;
    private String url;

    SharedPreferences Movie_Shared;
    SharedPreferences.Editor Movie_Editor;

    private int tabCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        result = (ListView)findViewById(R.id.result);
        result.setOnItemClickListener(this);
        //result = (TextView)findViewById(R.id.result);
        /*okHttp = (View)findViewById(R.id.okHttp);
        okHttp.setOnClickListener(this);*/

        Movie_Shared = getApplicationContext().getSharedPreferences("Movie_Shared", MODE_PRIVATE);
        Movie_Editor = Movie_Shared.edit();

        presenter = new CategoriesPresenterImpl(this, getApplicationContext());

        presenter.charge_dataBase();

        tabs = (TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        spec = tabs.newTabSpec("tabTopPopular");
        spec.setContent(R.id.tab_TopPopular);
        spec.setIndicator("Popular");
        tabs.addTab(spec);

        spec = tabs.newTabSpec("tabTopRate");
        spec.setContent(R.id.tab_TopRate);
        spec.setIndicator("Rate");
        tabs.addTab(spec);

        spec = tabs.newTabSpec("tab_TopUpcoming");
        spec.setContent(R.id.tab_TopRate);
        spec.setIndicator("Upcoming");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

        if(tabCount == 0){
            url = "https://api.themoviedb.org/3/movie/popular?language=es-ES&api_key=5757061b3d9b00c028b47d414976069e";
            presenter.getDataURL(url);
        }

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if(tabId.equals("tabTopPopular")) {
                    url = "https://api.themoviedb.org/3/movie/popular?language=es-ES&api_key=5757061b3d9b00c028b47d414976069e";
                    presenter.getDataURL(url);
                    tabCount = 1;
                }
                if(tabId.equals("tabTopRate")) {
                    url = "https://api.themoviedb.org/3/movie/top_rated?api_key=5757061b3d9b00c028b47d414976069e&language=es-ES&page=1";
                    presenter.getDataURL(url);
                    tabCount = 1;
                }
                if(tabId.equals("tab_TopUpcoming")) {
                    url = "https://api.themoviedb.org/3/movie/upcoming?api_key=5757061b3d9b00c028b47d414976069e&language=es-ES&page=1";

                    presenter.getDataURL(url);
                    tabCount = 1;
                }
            }
        });


    }

    /*@Override
    public void onClick(View v) {
        if(v.getId() == okHttp.getId()){
            presenter.getDataURL();
        }
    }*/

    @Override
    public void showProgress(boolean opt) {
        if(opt){
            result.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }
        else{
            progressBar.setVisibility(View.GONE);
            result.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showError(String Err) {
        Toast.makeText(Categories.this, Err, Toast.LENGTH_LONG).show();
    }

    /*@Override
    public void showResult(String Result) {
        result.setText(Result);
    }
    */

    @Override
    public void setItems(List<String> items) {
        result.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items));
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Movie_Editor.putInt("position",position);
        Movie_Editor.putString("url",url);
        Movie_Editor.commit();
        Intent j = new Intent(this, DetailsMovie.class);
        startActivity(j);
        //presenter.onItemClicked(position);
    }
}
