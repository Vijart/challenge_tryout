package movies.themoviedb.com.themoviedb.Interactores;



import android.app.ProgressDialog;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import movies.themoviedb.com.themoviedb.Entidades.Result;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesInteractor;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesPresenter;
import movies.themoviedb.com.themoviedb.Interfaces.wsServiceTheMovie;
import movies.themoviedb.com.themoviedb.SQLite.MoviesSQL;

/**
 * Created by Victor Reyes on 22/3/2017.
 */

public class CategoriesInteractorImpl implements CategoriesInteractor {

    private CategoriesPresenter presenter;
    private wsServiceTheMovie service;
    private OkHttpClient Client_Popular;
    private Request request;
    private Response response;
    private String[] result;
    private List ListItems;
    private ResponseTheMovie dataMovie;
    private Result resultMovie;
    private ProgressDialog pDialog;
    private SQLiteDatabase go_DataBase_Movie;
    private String gv_path_Movies = "data/data/movies.themoviedb.com/databases/MoviesDB";
    private MoviesSQL go_db_Movies;
    private Context ctx;
    private JSONArray arr, genres;

    public CategoriesInteractorImpl(CategoriesPresenter presenter, Context context){
        this.ctx = context;
        this.presenter = presenter;
    }

    @Override
    public void loading() {
        try{
            go_DataBase_Movie = SQLiteDatabase.openDatabase(gv_path_Movies, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }catch (SQLException e){
        }

        if(go_DataBase_Movie == null){
            //pDialog.setMessage("Database Does Not Exist");
            go_db_Movies = new MoviesSQL(ctx,"Movies", null, 1 );
            try {
                go_DataBase_Movie = go_db_Movies.getWritableDatabase();
            }catch (SQLException e){
                e.printStackTrace();
            }
            presenter.showMsgPresenter("Welcome, Database Created");
        }else{
            presenter.showMsgPresenter("Welcome, Database Exists");
        }

    }

    @Override
    public void findItems(final OnFinishedListener listener, final String URL ) {
        final Gson gson = new Gson();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        Client_Popular = new OkHttpClient();
                        request = new Request.Builder()
                                .url(URL)
                                .get()
                                .build();
                        try {
                            Response response = Client_Popular.newCall(request).execute();

                            String jsonData = response.body().string();
                            JSONObject Jobject = new JSONObject(jsonData);
                            dataMovie = new ResponseTheMovie();
                            dataMovie = gson.fromJson(String.valueOf(Jobject), ResponseTheMovie.class);
                            arr = Jobject.getJSONArray("results");
                            result = new String[arr.length()];
                            for (int i = 0; i < arr.length(); i++) {
                                result[i] = arr.getJSONObject(i).getString("original_title");
                                //dataMovie.setOriginalTitle(arr.getJSONObject(i).getString("original_title"));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Client_Popular = new OkHttpClient();
                        request = new Request.Builder()
                                .url("https://api.themoviedb.org/3/genre/movie/list?api_key=5757061b3d9b00c028b47d414976069e&language=es-ES")
                                .get()
                                .build();
                        try {
                            Response response = Client_Popular.newCall(request).execute();

                            String jsonData = response.body().string();
                            JSONObject Jobject = new JSONObject(jsonData);
                            genres = Jobject.getJSONArray("genres");
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void unused) {
                        String query;
                        int GenresMovie, GenresMaster;
                        go_db_Movies = new MoviesSQL(ctx,"Movies", null, 1 );
                        go_DataBase_Movie = go_db_Movies.getWritableDatabase();
                        if(go_DataBase_Movie != null){
                            for(int i = 0; i < dataMovie.getResults().size(); i++){
                                query = "INSERT INTO Movies(id, posterPath, overview, releaseDate, original_title, original_languaje, title, popularity, vote_average) " +
                                        "VALUES("+dataMovie.getResults().get(i).getId()+
                                        ", '"+dataMovie.getResults().get(i).getPosterPath()+"'" +
                                        ", '"+dataMovie.getResults().get(i).getOverview()+"'" +
                                        ", '"+dataMovie.getResults().get(i).getReleaseDate()+"'" +
                                        ", '"+dataMovie.getResults().get(i).getOriginalTitle()+"'" +
                                        ", '"+dataMovie.getResults().get(i).getOriginalLanguage()+"'" +
                                        ", '"+dataMovie.getResults().get(i).getTitle()+"'" +
                                        ", "+dataMovie.getResults().get(i).getPopularity()+"" +
                                        ", "+dataMovie.getResults().get(i).getVoteAverage()+")";
                                try {
                                    go_DataBase_Movie.execSQL(query);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                for (int k = 0; k< dataMovie.getResults().get(i).getGenreIds().size(); k++){
                                    GenresMovie = dataMovie.getResults().get(i).getGenreIds().get(k);
                                    for (int j = 0 ; j<genres.length(); j++){
                                        try {
                                            GenresMaster = genres.getJSONObject(j).getInt("id");
                                            if( GenresMovie == GenresMaster ){
                                                query = "INSERT INTO Genres(idMovie, genres_txt)" +
                                                        "VALUES("+dataMovie.getResults().get(i).getId()+"" +
                                                        ", '"+genres.getJSONObject(j).getString("name")+"')";
                                                try {
                                                    go_DataBase_Movie.execSQL(query);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }


                        }
                        ListItems =  new ArrayList();
                        ListItems = Arrays.asList(result);
                        listener.onFinished(ListItems);
                    }

                }.execute();
            }
        },3000);
    }
}
