package movies.themoviedb.com.themoviedb.Interfaces;

/**
 * Created by Victor Reyes on 25/3/2017.
 */

public interface DetailsMoviePresenter {

    void getDataPosition(int position, String URL);
}
