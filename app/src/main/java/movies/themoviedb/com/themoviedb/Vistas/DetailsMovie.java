package movies.themoviedb.com.themoviedb.Vistas;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import movies.themoviedb.com.themoviedb.Entidades.Result;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMoviePresenter;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMovieView;
import movies.themoviedb.com.themoviedb.Presentadores.DetailsMoviePresenterImp;
import movies.themoviedb.com.themoviedb.R;

public class DetailsMovie extends Activity implements DetailsMovieView {

    private TextView
            MovieTitle, MoviePoint, MovieOverview, tv_Sipnosis,
            MovieGenres;
    private ImageView poster, star;
    private ProgressBar progressBar;
    private DetailsMoviePresenter presenter;
    SharedPreferences Movie_Shared;
    SharedPreferences.Editor Movie_Editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_movie);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        Movie_Shared = getApplicationContext().getSharedPreferences("Movie_Shared", MODE_PRIVATE);
        Movie_Editor = Movie_Shared.edit();

        int pos = Movie_Shared.getInt("position",0);
        String url = Movie_Shared.getString("url","");


        MovieTitle = (TextView)findViewById(R.id.MovieTitle);
        MoviePoint = (TextView)findViewById(R.id.MoviePoint);
        poster = (ImageView)findViewById(R.id.im_Poster);
        star = (ImageView)findViewById(R.id.star);
        MovieGenres = (TextView)findViewById(R.id.MovieGenres);
        tv_Sipnosis = (TextView)findViewById(R.id.tv_Sipnosis);
        MovieOverview = (TextView)findViewById(R.id.MovieOverview);

        presenter = new DetailsMoviePresenterImp(this);

        presenter.getDataPosition(pos, url);
    }

    @Override
    public void showProgress(boolean opt) {
        if(opt){
            MovieTitle.setVisibility(View.GONE);
            MoviePoint.setVisibility(View.GONE);
            tv_Sipnosis.setVisibility(View.GONE);
            MovieOverview.setVisibility(View.GONE);
            MovieGenres.setVisibility(View.GONE);
            star.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

        }
        else{
            progressBar.setVisibility(View.GONE);
            MovieTitle.setVisibility(View.VISIBLE);
            MoviePoint.setVisibility(View.VISIBLE);
            tv_Sipnosis.setVisibility(View.VISIBLE);
            MovieOverview.setVisibility(View.VISIBLE);
            MovieGenres.setVisibility(View.VISIBLE);
            star.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setDetailsMovie(Result Data) {
        String GenreIds = "";

        MovieTitle.setText(Data.getOriginalTitle());
        MoviePoint.setText(String.valueOf(Data.getVoteAverage()));
        try{Picasso.with(this).load("https://image.tmdb.org/t/p/w500/"+Data.getPosterPath()).into(poster);}
        catch (Exception e){ e.printStackTrace();}
        MovieOverview.setText(Data.getOverview());
        for(int i = 0; i<Data.getGenreIds().size();i++){
            if(i == 0){
                GenreIds += ""+String.valueOf(Data.getGenreIds().get(i));
            }else{
                GenreIds += ", "+String.valueOf(Data.getGenreIds().get(i));
            }
        }
        MovieGenres.setText(GenreIds);
    }
}
