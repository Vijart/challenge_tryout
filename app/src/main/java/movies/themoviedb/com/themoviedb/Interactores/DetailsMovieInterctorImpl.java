package movies.themoviedb.com.themoviedb.Interactores;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import movies.themoviedb.com.themoviedb.Entidades.Result;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesPresenter;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMovieInteractor;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMoviePresenter;
import movies.themoviedb.com.themoviedb.Interfaces.wsServiceTheMovie;

import static android.content.ContentValues.TAG;

/**
 * Created by Victor Reyes on 25/3/2017.
 */

public class DetailsMovieInterctorImpl implements DetailsMovieInteractor {

    private DetailsMoviePresenter presenter;
    private wsServiceTheMovie service;
    private OkHttpClient Client_Popular;
    private Request request;
    private Response response;
    private String[] result;
    private List ListItems;
    private ResponseTheMovie dataMovie;
    private Result resultMovie;

    public DetailsMovieInterctorImpl(DetailsMoviePresenter presenter){
        this.presenter = presenter;
    }
    @Override
    public void findMoviePosition(final OnDetailsDataMovie listener, final int position, final String URL) {
        final Gson gson = new Gson();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        Client_Popular = new OkHttpClient();
                        request = new Request.Builder()
                                .url(URL)
                                .get()
                                .build();
                        try {
                            Response response = Client_Popular.newCall(request).execute();

                            String jsonData = response.body().string();
                            JSONObject Jobject = new JSONObject(jsonData);
                            dataMovie = new ResponseTheMovie();
                            dataMovie = gson.fromJson(String.valueOf(Jobject), ResponseTheMovie.class);
                            /*for (int i = 0; i < dataMovie.getResults().size(); i++) {
                                result[i] = arr.getJSONObject(i).getString("original_title");
                                //dataMovie.setOriginalTitle(arr.getJSONObject(i).getString("original_title"));
                            }*/
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void unused) {
                        listener.onDetails(dataMovie.getResults().get(position));
                    }

                }.execute();
            }
        },3000);
    }
}
