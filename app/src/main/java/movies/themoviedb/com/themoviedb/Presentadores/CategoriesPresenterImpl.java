package movies.themoviedb.com.themoviedb.Presentadores;

import android.content.Context;

import java.util.List;

import movies.themoviedb.com.themoviedb.Interactores.CategoriesInteractorImpl;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesInteractor;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesPresenter;
import movies.themoviedb.com.themoviedb.Interfaces.CategoriesView;

/**
 * Created by Victor Reyes on 22/3/2017.
 */

public class CategoriesPresenterImpl implements CategoriesPresenter, CategoriesInteractor.OnFinishedListener {

    private CategoriesView view;
    private CategoriesInteractor interactor;
    private Context ctx;

    public CategoriesPresenterImpl(CategoriesView view, Context context){
        this.view = view;
        this.ctx = context;
        interactor = new CategoriesInteractorImpl(this, ctx);
    }

    /*@Override
    public void showErrorPresenter(String Err) {
        if(view != null){
            view.showProgress(false);
            view.showError(Err);
        }
    }

    @Override
    public void showResultPresenter(String Result) {
        if(view != null){
            view.showProgress(false);
            //view.showResult(Result);
        }
    }*/

    @Override
    public void showMsgPresenter(String Toast) {
        if(view != null){
            view.showProgress(false);
            view.showMessage(Toast);
        }
    }

    @Override
    public void charge_dataBase() {
        if (view != null){
            view.showProgress(true);
            interactor.loading();
        }
    }

    @Override
    public void getDataURL(String URL) {
        if(view != null){
            view.showProgress(true);
            //view.showResult("");
            interactor.findItems(this, URL);
            //interactor.getDataURLInteractor(URL);
        }
    }

    @Override
    public void onItemClicked(int position) {
        if(view != null){
            view.showMessage(String.format("Position "+ position +"Clicked"));
        }
    }

    @Override
    public void onFinished(List<String> items) {
        if(view != null){
            view.setItems(items);
            view.showProgress(false);
        }
    }

   /* @Override
    public void webServiceURL(String URL) {
        if(view == null){
            view.showProgress();
        }else{
            interactor.HttpService(URL);
            view.hideProgress();
        }
    }*/
}
