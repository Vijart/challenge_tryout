package movies.themoviedb.com.themoviedb.Interfaces;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import movies.themoviedb.com.themoviedb.Entidades.Result;

/**
 * Created by Victor Reyes on 25/3/2017.
 */

public interface DetailsMovieView {

    void showProgress(boolean opt);
    //void fillPosterPath(String path);
    void setDetailsMovie(Result Data);
}
