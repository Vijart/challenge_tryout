package movies.themoviedb.com.themoviedb.Presentadores;

import movies.themoviedb.com.themoviedb.Entidades.ResponseTheMovie;
import movies.themoviedb.com.themoviedb.Entidades.Result;
import movies.themoviedb.com.themoviedb.Interactores.DetailsMovieInterctorImpl;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMovieInteractor;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMoviePresenter;
import movies.themoviedb.com.themoviedb.Interfaces.DetailsMovieView;

/**
 * Created by Victor Reyes on 25/3/2017.
 */

public class DetailsMoviePresenterImp implements DetailsMoviePresenter, DetailsMovieInteractor.OnDetailsDataMovie {

    private DetailsMovieView view;
    private DetailsMovieInteractor interactor;

    public DetailsMoviePresenterImp(DetailsMovieView view){

        this.view = view;
        interactor = new DetailsMovieInterctorImpl(this);
    }

    @Override
    public void getDataPosition(int position, String URL) {
        if(view != null ){
            view.showProgress(true);
            interactor.findMoviePosition(this, position, URL);
        }
    }

    @Override
    public void onDetails(Result results) {
        if(view != null){
            view.setDetailsMovie(results);
            view.showProgress(false);
        }
    }
}
