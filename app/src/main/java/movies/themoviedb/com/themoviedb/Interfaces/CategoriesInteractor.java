package movies.themoviedb.com.themoviedb.Interfaces;

import java.util.List;

/**
 * Created by Victor Reyes on 22/3/2017.
 */

public interface CategoriesInteractor {

    //void getDataURLInteractor(String URL);

    void loading();

    interface OnFinishedListener {
        void onFinished(List<String> items);
    }

    void findItems(OnFinishedListener listener, String URL);

   // void HttpService(String URL);
}
